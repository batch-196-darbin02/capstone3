// import {useState} from 'react';
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

	const {name, description, price, _id} = productProp;

	return(
		<Card className='mt-3 mb-3'>
			<Card.Body>
				<Card.Title>
					<h4>{name}</h4>
				</Card.Title>
				<Card.Subtitle>Product Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Product Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>
	)
}