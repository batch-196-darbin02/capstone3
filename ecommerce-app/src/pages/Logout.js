import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	// localStorage.clear(); // no need for this because there's already a value from provider

	// Placing the 'setUser' function inside of a useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different component

	// By adding useEffect, this will allow the logout page to render first before trigerring the useEffect which chages the state of the user
	useEffect(() => {

		// Set the user state back to its origin state
		setUser({id: null})
	});

	return(
		<Navigate to='/login'/>


	)
};