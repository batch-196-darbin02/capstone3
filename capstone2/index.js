// SECTION I. Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

// SECTION II. Server Setup
const app = express();
const port = 4000;

app.use(express.json());
//middleware
app.use(cors());

// SECTION III. Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.6dj5p.mongodb.net/capstone3?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB"));


// SECTION IV. Backend Routes
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);
const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders', orderRoutes);


// SECTION V. Server Gateway Response
app.listen(port,()=>console.log(`Server is running at port ${port}`));