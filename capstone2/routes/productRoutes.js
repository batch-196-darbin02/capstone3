// SECTION I. Dependencies
const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

// SECTION II. Add Product Route
router.post('/',verify,verifyAdmin,productControllers.addProduct);

// SECTION III. View All Active Product
router.get('/viewAllActive',productControllers.viewAllActive);

// SECTION IV. View A Product
router.get('/viewOne/:productId',productControllers.viewOne);

// SECTION V. Update Product Information
router.put('/update/:productId',verify,verifyAdmin,productControllers.updateProduct);

// SECTION VI. Archive A Product
router.delete('/archive/:productId',verify,verifyAdmin,productControllers.archiveProduct);

// SECTION VII. Activate A Product
router.put('/activate/:productId',verify,verifyAdmin,productControllers.activateProduct);

// SECTION ?. Router Export
module.exports = router;